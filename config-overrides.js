const {
  override,
  addBabelPlugins,
  addDecoratorsLegacy,
  disableEsLint
} = require("customize-cra");

module.exports = override(
  addDecoratorsLegacy(),
  disableEsLint(),
  ...addBabelPlugins([
    "module-resolver",
    {
      root: ["./src"],
      alias: {}
    }
  ])
);
