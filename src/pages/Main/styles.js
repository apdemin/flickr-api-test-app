import CircularProgress from "@material-ui/core/CircularProgress";
import InputBase from "@material-ui/core/InputBase";
import { fade } from "@material-ui/core/styles/colorManipulator";
import MUISearchIcon from "@material-ui/icons/Search";
import React from "react";
import styled from "styled-components";

export const Search = styled.div`
  position: relative;
  border-radius: ${props => props.theme.shape.borderRadius}px;
  background-color: ${props => fade(props.theme.palette.common.white, 0.15)};
  &:hover {
    background-color: ${props => fade(props.theme.palette.common.white, 0.25)};
  }
  margin-right: ${props => props.theme.spacing.unit * 2}px;
  margin-left: ${props => props.theme.spacing.unit * 2}px;
  width: 100%;
  ${props => props.theme.breakpoints.up("sm")} {
    font-size: 16px;
    margin-left: auto;
    width: auto;
  }
`;

export const SearchIcon = styled(({ className }) => (
  <div className={className}>
    <MUISearchIcon />
  </div>
))`
  width: ${props => props.theme.spacing.unit * 9}px;
  height: 100%;
  position: absolute;
  pointer-events: none;
  display: flex;
  align-items: center;
  justify-content: center;
`;

export const SearchInput = styled(InputBase)`
  && {
    .root {
      color: inherit;
      width: 100%;
    }
    input {
      padding-top: ${props => props.theme.spacing.unit}px;
      padding-right: ${props => props.theme.spacing.unit}px;
      padding-bottom: ${props => props.theme.spacing.unit}px;
      padding-left: ${props => props.theme.spacing.unit * 10}px;
      transition: ${props => props.theme.transitions.create("width")};
      width: 100%;
      ${props => props.theme.breakpoints.up("sm")} {
        width: 200px;
        &:focus {
          width: 300px;
        }
      }
    }
  }
`;
export const Page = styled.div`
  height: 100vh;
  display: flex;
  flex-direction: column;
  overflow: hidden;
`;

export const SelectedTags = styled.div`
  text-align: left;
  padding-bottom: ${props => props.theme.spacing.unit * 2}px;
  h6 {
    display: inline-block;
  }
`;

export const Container = styled.div`
  flex-grow: 1;
  text-align: center;
  padding: ${props => props.theme.spacing.unit * 2}px;
  overflow-y: scroll;
`;

export const ProgressSpinner = styled(({ className }) => (
  <div className={className}>
    <CircularProgress />
  </div>
))`
  padding-top: ${props => props.theme.spacing.unit * 2}px;
`;
