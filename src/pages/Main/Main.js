import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import PhotoList from "components/PhotoList";
import TagList from "components/TagList";
import { observer } from "mobx-react";
import React, { Component } from "react";
import InfiniteScroll from "react-infinite-scroller";
import feedStore from "store";
import { debounce } from "utils";
import {
  Container,
  Page,
  ProgressSpinner,
  Search,
  SearchIcon,
  SearchInput,
  SelectedTags
} from "./styles";

@observer
class MainPage extends Component {
  searchPhoto = debounce(feedStore.searchPhoto, 500);

  onSearch = event => {
    this.searchPhoto(event.target.value);
  };

  render() {
    return (
      <Page>
        <AppBar position="static">
          <Toolbar>
            <Typography variant="h6" color="inherit">
              Flickr&nbsp;Photo&nbsp;Stream
            </Typography>
            <Search>
              <SearchIcon />
              <SearchInput placeholder="Search…" onChange={this.onSearch} />
            </Search>
          </Toolbar>
          {feedStore.selectedTags.length > 0 && (
            <Toolbar>
              <SelectedTags>
                <Typography variant="h6" color="inherit">
                  Selected tags:&nbsp;
                </Typography>
                <TagList tags={feedStore.selectedTags.join(" ")} />
              </SelectedTags>
            </Toolbar>
          )}
        </AppBar>
        <Container>
          <InfiniteScroll
            pageStart={1}
            initialLoad={true}
            loadMore={feedStore.fetchPhotos}
            hasMore={feedStore.hasMore}
            useWindow={false}
            loader={null}
          >
            <PhotoList photos={feedStore.photos} key="photos" />
          </InfiniteScroll>
          {feedStore.isFetching && <ProgressSpinner />}
        </Container>
      </Page>
    );
  }
}

export default MainPage;
