export function debounce(targetFunction, timeout) {
  let timer = null;
  return function(...args) {
    const onComplete = () => {
      targetFunction.apply(this, args);
      timer = null;
    };

    if (timer) {
      clearTimeout(timer);
    }

    timer = setTimeout(onComplete, timeout);
  };
}
