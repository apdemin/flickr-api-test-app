import { applySnapshot, flow, getSnapshot, types } from "mobx-state-tree";
import * as api from "api";

function mutatePhotoData(data) {
  const { height_m, description, url_m, width_m, ...other } = data;
  return {
    height: +height_m,
    width: +width_m,
    description: description._content || "",
    url: url_m || "",
    ...other
  };
}

const PhotoItem = types
  .model({
    description: types.optional(types.string, ""),
    url: types.string,
    height: types.number,
    width: types.number,
    id: types.identifier,
    owner: types.optional(types.string, ""),
    ownername: types.optional(types.string, ""),
    tags: types.optional(types.string, ""),
    title: types.optional(types.string, "")
  })
  .views(self => ({
    get externalUrl() {
      return `https://www.flickr.com/photos/${self.owner}/${
        self.id
      }/in/dateposted/`;
    },
    get ownerUrl() {
      return `https://www.flickr.com/photos/${self.owner}/`;
    }
  }));

const FeedStore = types
  .model({
    page: types.number,
    pages: types.maybeNull(types.number),
    perpage: types.number,
    photos: types.optional(types.array(PhotoItem), []),
    isFetching: types.boolean,
    error: types.string,
    searchText: types.string,
    selectedTags: types.optional(types.array(types.string), [])
  })
  .views(self => ({
    get hasMore() {
      return self.page <= self.pages;
    }
  }))
  .actions(self => ({
    clearError: () => (self.error = ""),
    toggleTag: tag => {
      if (self.selectedTags.includes(tag)) {
        self.selectedTags = self.selectedTags.filter(item => item !== tag);
      } else self.selectedTags.push(tag);
      console.log(self.selectedTags.join(" "));

      self.refreshFeed();
    },
    searchPhoto: text => {
      self.searchText = text;
      self.refreshFeed();
    },
    refreshFeed: () => {
      self.page = initialStoreValues.page;
      self.pages = initialStoreValues.pages;
      self.photos = [];
      self.fetchPhotos();
    },
    fetchPhotos: flow(function*() {
      if (self.isFetching) return false;
      self.isFetching = true;
      try {
        const { page, pages, perpage, photo } = yield api.searchImages({
          page: self.page,
          per_page: self.perpage,
          text: self.searchText,
          tags: getSnapshot(self).selectedTags
        });
        const { photos, ...other } = getSnapshot(self);
        const existsIds = photos.map(item => item.id);
        applySnapshot(self, {
          ...other,
          pages,
          page: page + 1,
          perpage,
          photos: [
            ...photos,
            ...photo
              .filter(item => !existsIds.includes(item.id))
              .map(mutatePhotoData)
          ]
        });
      } catch (error) {
        console.error("Failed to fetch photos", error);
        self.error = error;
      }
      self.isFetching = false;
      return true;
    })
  }));

const initialStoreValues = {
  page: 1,
  perpage: 24,
  pages: 1,
  isFetching: false,
  error: "",
  searchText: ""
};
const feedStore = FeedStore.create(initialStoreValues);

export default feedStore;
