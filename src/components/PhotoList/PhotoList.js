import React, { Component } from "react";
import PhotoListItem from "components/PhotoListItem";
import Grid from "@material-ui/core/Grid";
import { observer } from "mobx-react";

@observer
class PhotoList extends Component {
  render() {
    const { photos } = this.props;
    return (
      <Grid container spacing={24}>
        {photos.map(photo => (
          <PhotoListItem photo={photo} key={photo.id} />
        ))}
      </Grid>
    );
  }
}

export default PhotoList;
