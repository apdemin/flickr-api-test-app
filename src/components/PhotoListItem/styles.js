import React from "react";
import styled from "styled-components";
import Paper from "@material-ui/core/Paper";
import Img from "react-image";
import { CircularProgress } from "@material-ui/core";

export const Item = styled(Paper)`
  min-height: 100%;
  box-sizing: border-box;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: space-between;
  padding: ${props => props.theme.spacing.unit * 2}px;
  color: ${props => props.theme.palette.text.secondary};
`;

export const Photo = styled(props => (
  <Img {...props} loader={<CircularProgress />} />
))`
  max-width: 100%;
  max-height: 100%;
`;

export const PhotoContainer = styled.div`
  text-align: center;
  position: relative;
  overflow: hidden;
  box-sizing: border-box;
  flex: 1;
`;

export const Info = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;
  text-align: left;
  max-height: 50%;
  padding-top: ${props => props.theme.spacing.unit}px;
  box-sizing: border-box;
`;

export const Links = styled.span`
  max-width: 100%;
  overflow: hidden;
  text-overflow: ellipsis;
`;

export const Description = styled.div`
  flex: 1;
  overflow: hidden;
  text-overflow: ellipsis;
  padding-top: ${props => props.theme.spacing.unit}px;
`;

export const Tags = styled.div`
  max-width: 100%;
  text-overflow: ellipsis;
  overflow: hidden;
  padding-top: ${props => props.theme.spacing.unit}px;
`;
