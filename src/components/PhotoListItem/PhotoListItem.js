import React, { Component } from "react";
import Grid from "@material-ui/core/Grid";
import TagList from "components/TagList";
import {
  Description,
  Info,
  Item,
  Links,
  Photo,
  PhotoContainer,
  Tags
} from "./styles";

class PhotoListItem extends Component {
  render() {
    const {
      url,
      title,
      ownerUrl,
      ownername,
      externalUrl,
      description,
      tags
    } = this.props.photo;
    return url ? (
      <Grid item xs={12} sm={6} md={4} lg={3}>
        <Item>
          <PhotoContainer>
            <Photo src={url} alt={title} />
          </PhotoContainer>
          <Info>
            <Links>
              <a href={externalUrl} target="_blank">
                {title || "Photo"}
              </a>
              &nbsp;by&nbsp;
              <a href={ownerUrl} target="_blank">
                {ownername}
              </a>
            </Links>
            <Description dangerouslySetInnerHTML={{ __html: description }} />
            {tags && (
              <Tags>
                <b>Tags:&nbsp;</b>
                <TagList tags={tags}/>
              </Tags>
            )}
          </Info>
        </Item>
      </Grid>
    ) : null;
  }
}

export default PhotoListItem;
