import React, { Component } from "react";
import styled from "styled-components";
import Chip from "@material-ui/core/Chip";
import feedStore from "store";
import { observer } from "mobx-react";

@observer
class TagList extends Component {
  render() {
    const { tags = "" } = this.props;
    return (
      <React.Fragment>
        {tags
          .split(" ")
          .map(
            tag =>
              tag && (
                <Tag
                  key={tag}
                  tag={tag}
                  onToggle={feedStore.toggleTag}
                  selected={feedStore.selectedTags.includes(tag)}
                />
              )
          )}
      </React.Fragment>
    );
  }
}

const Tag = ({ onToggle, tag, selected }) => {
  const onClick = () => onToggle(tag);
  return (
    <StyledTag
      label={tag}
      onClick={onClick}
      color={selected ? "secondary" : undefined}
      onDelete={selected ? onClick : null}
    />
  );
};

export const StyledTag = styled(Chip)`
  margin: 0 4px 4px 0;
`;

export default TagList;
