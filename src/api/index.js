//@global fetch
import qs from "qs";
const API_HOST = "https://api.flickr.com/";
const API_KEY = "ed635fd4abe4fdd5277071acf27ef548";
const REST_ROOT = `${API_HOST}services/rest/`;

const DEFAULT_PARAMS = {
  api_key: API_KEY,
  format: "json",
  nojsoncallback: 1
};

export async function searchImages(options) {
  const params = {
    ...DEFAULT_PARAMS,
    ...options,
    tag_mode: "all",
    privacy_filter: 1,
    safe_search: 1, // force safe mode
    sort: "date-posted-desc",
    extras: ["tags", "description", "owner_name", "url_m", "path_alias"],
    method: "flickr.photos.search"
  };

  const api_uri = `${REST_ROOT}?${qs.stringify(params, {
    arrayFormat: "comma"
  })}`;
  return fetch(api_uri, {
    method: "GET"
  })
    .then(response => response.json())
    .then(data => data.photos);
}
