This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

Data store managed with [MobX](https://github.com/mobxjs/mobx) and [mobx-state-tree](https://github.com/mobxjs/mobx-state-tree)

Styled with [material-ui](https://github.com/mui-org/material-ui) and [styled-components](https://github.com/styled-components/styled-components)

Flickr REST API was used instead of Flickr public feeds because REST API have necessary params for pagination, safe mode filtration etc

## Available Scripts

Before starting project you need to install `node` to your machine 
and then all dependencies trough `npm install` or `yarn`

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.

### `npm run build`

Builds the app for production to the `build` folder.<br>
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br>
Your app is ready to be deployed!
